import org.apache.spark.storage.StorageLevel._;

val fileRDD = sc.textFile("/tests/yahoo_stocks.csv");

fileRDD.persist(MEMORY_ONLY);

System.out.println("Count of RDD (first run): "+fileRDD.count());

System.out.println("Count of RDD (second run): "+fileRDD.count());

