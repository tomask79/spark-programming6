# Caching the Apache Spark RDD - performance tunning #

Sometimes you want process RDD more then once, in **more then one RDD actions**.  
This bring up a major problem...Spark **will always hit the disk for the data**.  
So you will be dealing with performance issue.

**Solution:**

RDD comes with **cache()** and **persist()** methods  
where cache() = persist(StorageLevel.MEMORY_ONLY).  
Mentioned methods will help us to save the RDD into the memory.  
Read more about [other options](http://spark.apache.org/docs/latest/rdd-programming-guide.html#rdd-persistence).  

Let's test it on simple example:

	import org.apache.spark.storage.StorageLevel._;

	val fileRDD = sc.textFile("/tests/yahoo_stocks.csv");

	fileRDD.persist(MEMORY_ONLY);

	System.out.println("Count of RDD (first run): "+fileRDD.count());

	System.out.println("Count of RDD (second run): "+fileRDD.count());

(download the yahoo_stocks.csv from https://github.com/dmatrix/examples/blob/master/spark/hdp/data/yahoo_stocks.csv)

Now let me tell you what is happening:

**Without persist(MEMORY_ONLY) call**, first invoke of fileRDD.count() would trigger the data fetching from disk
and counting of rows. Second hit of fileRDD.count() would mean the same thing.

**With persist(MEMORY_ONLY) call**, we're telling to Spark:  

do LAZYLY

* read the file
* cache the RDD to memory

Now second invocation of fileRDD.count() should be much faster then the first one, 
because it won't use the disk as source for counting stuff (of course if RDD will fit into the memory)  

Let's prove it (run):

	spark-shell -i test14.scala

First fileRDD.count() run output:

	18/02/17 22:21:10 INFO DAGScheduler: Job 0 finished: count at <console>:33,  
	took 1,425495 s
	Count of RDD (first run): 4794

Second fileRDD.count() run output:

	18/02/17 22:21:11 INFO DAGScheduler: Job 1 finished: count at <console>:33,  
	took 0,121783 s
	Count of RDD (second run): 4794

Look at the huge speed up of the second RDD action.  

regards

Tomas